from django.core.management.base import BaseCommand
from lib.utils import upload_initial_data


class Command(BaseCommand):
    """Management command."""

    help = 'upload initial data'

    def handle(self, *args, **options):
        """Handle command."""
        upload_initial_data()
        self.stdout.write(self.style.SUCCESS('Successfully uploaded data.'))
