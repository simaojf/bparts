#!/usr/bin/env bash

# run python static validation
prospector  --profile-path=. --profile=.prospector.yml --path=src

# run bandit - A security linter from OpenStack Security
bandit -r src
