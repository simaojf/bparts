import factory

from django.test import TestCase

from cars.models import CarBrand, CarModel, CarVersion, CarPart, Product


class CarBrandFactory(factory.DjangoModelFactory):

    class Meta:
        model = CarBrand


class CarModelFactory(factory.DjangoModelFactory):

    class Meta:
        model = CarModel


class CarVersionFactory(factory.DjangoModelFactory):

    class Meta:
        model = CarVersion


class CarPartFactory(factory.DjangoModelFactory):

    class Meta:
        model = CarPart


class ProductFactory(factory.DjangoModelFactory):

    class Meta:
        model = Product


class CarsModelsTests(TestCase):

    def setUp(self):
        self.brand = CarBrandFactory.create(name='Mercedes')
        self.model = CarModelFactory.create(name='E')
        self.version = CarVersionFactory.create(name='270')
        self.part = CarPartFactory.create(name='engine')
        self.product = ProductFactory.create(
            brand=self.brand,
            model=self.model,
            version=self.version,
            part=self.part,
            price='675'
        )

    def test_string(self):
        """Check if str is correct."""
        self.assertEqual(str(self.brand), 'Mercedes')
        self.assertEqual(str(self.model), 'E')
        self.assertEqual(str(self.version), '270')
        self.assertEqual(str(self.part), 'engine')
        self.assertEqual(str(self.product), 'Mercedes E 270')
