from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from .test_models import CarBrandFactory, CarModelFactory, CarVersionFactory, \
    CarPartFactory, ProductFactory


class SearchPartsTests(APITestCase):
    """Car parts search."""

    def setUp(self):
        """Set up data."""

        self.brand = CarBrandFactory.create(name='Mercedes')
        self.model_e = CarModelFactory.create(name='E')
        self.model_c = CarModelFactory.create(name='C')
        self.version_270 = CarVersionFactory.create(name='270')
        self.version_200 = CarVersionFactory.create(name='200')
        self.part_engine = CarPartFactory.create(name='engine')
        self.part_wheel = CarPartFactory.create(name='wheel')

        self.product_a = ProductFactory.create(
            brand=self.brand,
            model=self.model_e,
            version=self.version_270,
            part=self.part_engine,
            price='675'
        )
        self.product_b = ProductFactory.create(
            brand=self.brand,
            model=self.model_e,
            version=self.version_200,
            part=self.part_engine,
            price='500'
        )
        self.product_c = ProductFactory.create(
            brand=self.brand,
            model=self.model_c,
            version=self.version_270,
            part=self.part_wheel,
            price='150'
        )

    def test_search_fail(self):
        """Test cases of failures"""
        url = reverse('products')

        # Method not allowed.
        response = self.client.post(url, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # Id must be int.
        data = {
            'brand_id': 'test'
        }
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_search(self):
        """Tests success."""
        url = reverse('products')

        # Search by one param.
        data = {
            'brand_id': self.brand.pk
        }
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['products']), 3)

        data = {
            'part_id': self.part_wheel.pk
        }
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['products']), 1)

        # Search by more than one param.
        data = {
            'brand_id': self.brand.pk,
            'model_id': self.model_e.pk,
            'version_id': self.version_200.pk
        }
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['products']), 1)

        # Search but not find.
        data = {
            'brand_id': self.brand.pk,
            'model_id': self.model_e.pk,
            'version_id': self.version_200.pk,
            'part_id': self.part_wheel.pk
        }
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['products']), 0)

        # Full search.
        data = {
            'brand_id': self.brand.pk,
            'model_id': self.model_e.pk,
            'version_id': self.version_200.pk,
            'part_id': self.part_engine.pk
        }
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['products']), 1)
