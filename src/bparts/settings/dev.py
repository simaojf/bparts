from bparts.settings.base import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = ')kpo*j)g&_whb+%fz58nqlb1#g9bpyu9o@%tb049zop3c37%=i'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
