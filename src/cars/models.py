from decimal import Decimal

from django.db import models
from django.core.validators import MinValueValidator


class CarBrand(models.Model):
    """Model for cars brands."""

    name = models.CharField('name', max_length=150)

    def __str__(self):
        """Object representation."""
        return self.name


class CarModel(models.Model):
    """Model for cars models."""

    name = models.CharField('name', max_length=250)

    def __str__(self):
        """Object representation."""
        return self.name


class CarVersion(models.Model):
    """Model for cars versions."""

    name = models.CharField('name', max_length=250)

    def __str__(self):
        """Object representation."""
        return self.name


class CarPart(models.Model):
    """Model for cars parts."""

    name = models.CharField('name', max_length=250)

    def __str__(self):
        """Object representation."""
        return self.name


class Product(models.Model):
    """Model for products."""

    brand = models.ForeignKey(
        CarBrand, verbose_name='brand', on_delete=models.PROTECT
    )
    model = models.ForeignKey(
        CarModel, verbose_name='model', on_delete=models.PROTECT
    )
    version = models.ForeignKey(
        CarVersion, verbose_name='version', on_delete=models.PROTECT
    )
    part = models.ForeignKey(
        CarPart, verbose_name='part', on_delete=models.PROTECT
    )
    price = models.DecimalField(
        'price', null=True, max_digits=8, decimal_places=2,
        validators=[MinValueValidator(Decimal('0'))]
    )

    def __str__(self):
        """Object representation."""
        return '{} {} {}'.format(self.brand, self.model, self.version)

    @property
    def to_json(self):
        """Object representation in json."""
        return {
            'part_name': self.part.name,
            'price': str(self.price),
            'for_car': str(self)
        }
