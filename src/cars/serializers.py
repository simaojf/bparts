from rest_framework import serializers


class SearchProductSerializer(serializers.Serializer):
    """Serializer to search products."""

    brand_id = serializers.IntegerField(required=False, source='brand')
    model_id = serializers.IntegerField(required=False, source='model')
    version_id = serializers.IntegerField(required=False, source='version')
    part_id = serializers.IntegerField(required=False, source='part')

    def create(self, validated_data):  # pragma: no cover
        """Create function."""
        pass

    def update(self, instance, validated_data):  # pragma: no cover
        """Update function."""
        pass
