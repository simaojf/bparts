from django.urls import path

import cars.api

urlpatterns = [
    path('product/', cars.api.ProductAPI.as_view(), name='products'),
]
