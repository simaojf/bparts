from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from cars.models import Product
from cars.serializers import SearchProductSerializer


class ProductAPI(APIView):
    """List products."""

    def get(self, request):
        """
        Return product list.

        Filter by one or more params (brand_id, model_id, etc..)
        """
        data = {}
        serializer = SearchProductSerializer(data=self.request.query_params)
        if serializer.is_valid(raise_exception=True):
            # Create search query using all valid arguments.
            query = {}
            for key, value in serializer.validated_data.items():
                query.update({key: value})

            # Filter results.
            products = Product.objects.filter(**query)
            data['products'] = [product.to_json for product in products]

        return Response(data=data, status=status.HTTP_200_OK)
