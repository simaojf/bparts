import csv
from decimal import Decimal, InvalidOperation

from django.db import transaction
from cars.models import CarBrand, CarModel, CarVersion, CarPart, Product


@transaction.atomic
def upload_initial_data():
    """Upload initial data."""

    print('----UPLOAD INITIAL DATA----')

    print('Upload car brands: ', sep=' ', end='', flush=True)
    upload_common_files(CarBrand, 'uploaded_files/brands.csv')

    print('Upload car models: ', sep=' ', end='', flush=True)
    upload_common_files(CarModel, 'uploaded_files/models.csv')

    print('Upload car versions: ', sep=' ', end='', flush=True)
    upload_common_files(CarVersion, 'uploaded_files/versions.csv')

    print('Upload car parts: ', sep=' ', end='', flush=True)
    upload_common_files(CarPart, 'uploaded_files/parts.csv')

    print('Upload products: ', sep=' ', end='', flush=True)
    upload_products('uploaded_files/products.csv')


@transaction.atomic
def upload_common_files(model, file_path):
    """Upload common files."""

    fieldnames = [
        'id',
        'name'
    ]
    with open(file_path, 'r') as csv_file:
        reader = csv.DictReader(csv_file, fieldnames=fieldnames, delimiter=',')
        next(reader, None)  # has header
        model.objects.bulk_create(
            [model(id=row['id'], name=row['name']) for row in reader]
        )

    print('Done!')
    return True


@transaction.atomic
def upload_products(file_path):
    """Upload products."""

    fieldnames = [
        'id',
        'price',
        'part',
        'brand',
        'model',
        'version'
    ]
    with open(file_path, 'r') as csv_file:
        reader = csv.DictReader(csv_file, fieldnames=fieldnames, delimiter=',')
        next(reader, None)  # has header

        for row in reader:
            # Some prices aren't defined on .csv file.
            try:
                price = Decimal(row['price'])
            except InvalidOperation:
                price = None

            # I assume all id's on the file are correct and match
            # the other files.
            Product.objects.create(
                id=row['id'],
                brand_id=row['brand'],
                model_id=row['model'],
                version_id=row['version'],
                part_id=row['part'],
                price=price
            )

    print('Done!')
    return True
