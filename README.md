# Car parts

Car parts search.

## Retrieve code

* `$ git clone https://simaojf@bitbucket.org/simaojf/bparts.git`

## Installation

* `$ virtualenv -p /usr/bin/python3 .`
* `$ source bin/activate`
* `$ cd bparts`
* `$ pip install -r requirements/dev.txt`
* `$ cd src`
* `$ python manage.py migrate`
* `$ python manage.py upload_data`

## Running

Run Django development http server

* `$ python manage.py runserver`

## Testing

Backend (django/python tests)

* `$ ./scripts/test_local_backend.sh`

## Static analysis (code style and security)

* `$ ./scripts/static_validate_backend.sh`

## NOTES

* For coverage results, after running the test_local_backend script, open index.html from htmlcov folder.
